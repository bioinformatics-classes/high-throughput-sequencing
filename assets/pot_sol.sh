#!/bin/bash

REFERENCE=$1
ASSEMBLY=$2

# Obrian average coverage:
samtools depth ${ASSEMBLY} | cut -f 3 | paste -sd+ - | bc

# Obtain position with highest coverage
samtools depth ${ASSEMBLY}| sort -n -k 3 | tail -n 1 | cut -f 2

# Obtain the average coverage of the 200 base pairs around the highest coverage position (100 each side)
samtools depth ${ASSEMBLY}| grep -C 100 -w "20694" | cut -f 3 |  paste -sd+ - | bc
## Alternative (non hard-coded):
samtools depth ${ASSEMBLY}| grep -C 100 -w $(samtools depth ${ASSEMBLY} | sort -n -k 3 | tail -n 1 | cut -f 2) | cut -f 3 |  paste -sd+ - | bc

# Perform a basic SNP calling on your assembled genome 
bcftools mpileup --fasta-ref ${REFERENCE} ${ASSEMBLY}| bcftools call -m -v - > My_variants.vcf

