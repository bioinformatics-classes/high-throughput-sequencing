### Classes[12] = "High Throughput Sequencing"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

---

### Summary

* Sequence representation reminder
* Sanger sequence data
* High Throughput data
    * Technologies
* Assemblies
    * Practical examples

---

### Sanger sequencing

* &shy;<!-- .element: class="fragment" -->Primer based
* &shy;<!-- .element: class="fragment" -->Up to 1Kbp reads
* &shy;<!-- .element: class="fragment" -->Large files
* &shy;<!-- .element: class="fragment" -->"Low" throughput  
    * &shy;<!-- .element: class="fragment" -->~40 min per run
    * &shy;<!-- .element: class="fragment" -->96 plex
    * &shy;<!-- .element: class="fragment" -->144Kbp per hour
    * &shy;<!-- .element: class="fragment" -->3.5Mbp per day

<img src="assets/chroma.png" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### High throughput sequencing

* &shy;<!-- .element: class="fragment" -->What if we need to scale things?
    * &shy;<!-- .element: class="fragment" -->(And not spend our entire budget on sequencing)
* &shy;<!-- .element: class="fragment" -->Unknown genome regions
    * &shy;<!-- .element: class="fragment" -->Non-model organisms
* &shy;<!-- .element: class="fragment" -->We need more power

<a href="https://what-if.xkcd.com/13/"><img class="fragment" src="assets/laser_pointer_more_power.png" style="background:none; border:none; box-shadow:none;"></a>

---

### Scaling things up

&shy;<!-- .element: class="fragment" --> ![Sequencing techs plot](assets/developments_in_high_throughput_sequencing.png)

---

### Amplification based tech

&shy;<!-- .element: class="fragment" --> ![Amplification based technologies scheme](assets/HTS_Amplifications.jpg)

|||

### Single molecule tech

&shy;<!-- .element: class="fragment" --> ![Single molecule based technologies scheme](assets/HTS_Single.jpg)

---

### Scaling things up

&shy;<!-- .element: class="fragment" --> ![Sequencing techs plot](assets/developments_in_high_throughput_sequencing.png)

|||

### No more chromatograms for you!

* &shy;<!-- .element: class="fragment" -->Sequences of 1Kbp
* &shy;<!-- .element: class="fragment" -->1000 reads provide ~1Mbp
* &shy;<!-- .element: class="fragment" -->Each chromatogram takes 100 ~ 200 KB
    * &shy;<!-- .element: class="fragment" -->1000 reads (1Mbp) * 150KB = 150MB
    * &shy;<!-- .element: class="fragment" -->100000 reads (100Mbp) * 150KB = 15GB

&shy;<!-- .element: class="fragment" --> ![Burning HDD](assets/Disk_full.png)

|||

### Enter the [FASTQ](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/FileFormat_FASTQ-files_swBS.htm) [format](https://pythonhosted.org/OBITools/fastq.html)

* &shy;<!-- .element: class="fragment" -->Like a FASTA file, but with quality scores per base
* &shy;<!-- .element: class="fragment" -->Each sequence is composed of 4 lines:

<div class="fragment">

```text
@Sequence_identifier
ATGCGATAGCTGACTGACTAGCT
+(Seq_ID again)
!''*(((((******+***,-
```

</div>

&shy;<!-- .element: class="fragment" --> ![Sequence quality values vs. error probability plot](assets/Probabilitymetrics.png)

|||

### FASTQ format

```bash
# Obtain a fastq sequence file
cd ~/
mkdir hts_data
cd hts_data
wget https://gitlab.com/bioinformatics-classes/high-throughput-sequencing/-/raw/master/assets/short_reads.fastq

# Look at the file contents
nano short_reads.fastq

# How much space does this file occupy?
# How many sequences are represented?
# How many base pairs do you estimate? (you may want to answer this later)

```

---

### Quality Control

* &shy;<!-- .element: class="fragment" -->QC is performed massively
* &shy;<!-- .element: class="fragment" -->[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)

&shy;<!-- .element: class="fragment" --> ![Sequence quality values vs. error probability plot](assets/Probabilitymetrics.png)

|||

### Quality Control

``` bash
conda create -n hts
conda activate hts
conda install fastqc

fastqc

fastqc /path/to/your/file.fastq
```

---

### Assemblies

* &shy;<!-- .element: class="fragment" -->Sequence assemblies are a huge problem
    * &shy;<!-- .element: class="fragment" -->HTS reads come from random genome locations
    * &shy;<!-- .element: class="fragment" -->We could use an entire semester to deal with this problem
* &shy;<!-- .element: class="fragment" -->Two types of sequence assembly
    * &shy;<!-- .element: class="fragment" -->*Mapping* assemblies (reference available)
    * &shy;<!-- .element: class="fragment" -->*Denovo* assemblies (reference unavailable)

|||

### Assemblies

&shy;<!-- .element: class="fragment" --> ![HTS assembly plot](assets/NGS-image.png)

|||

### Assemblies

&shy;<!-- .element: class="fragment" --> ![A puzzle box](assets/Puzzle_and_box.jpg)

|||

### Assemblies

&shy;<!-- .element: class="fragment" --> ![A boxless puzzle](assets/Puzzle_no_box.jpg)

|||

### Assemblies

&shy;<!-- .element: class="fragment" --> ![An assembled white puzzle](assets/krypt-puzzle-image.jpg)

|||

### Assemblies

&shy;<!-- .element: class="fragment" --> ![Multiple boxed puzzles](assets/EST_puzzle.jpg)

|||

### Assemblies

&shy;<!-- .element: class="fragment" --> ![Mismatching puzzle pieces](assets/mismatch.jpg)

---

### A reference sequence is available

* &shy;<!-- .element: class="fragment" -->A reference sequence solves most assembling issues
    * &shy;<!-- .element: class="fragment" -->We just need to align our reads to the reference "backbone"
    * &shy;<!-- .element: class="fragment" -->There are plenty of algorithms for this
        * &shy;<!-- .element: class="fragment" -->Bowtie2
        * &shy;<!-- .element: class="fragment" -->BWA

|||

### Map it yourself!

```bash
# Make sure you are in the right environment
conda activate hts

# Install the required tools:
conda install bowtie2 samtools tablet

# Get a reference sequence
cd ~/hts_data
wget https://gitlab.com/bioinformatics-classes/high-throughput-sequencing/-/raw/master/assets/reference.fasta

# Now, let the magic begin.
# First we need to create an index from our reference sequence and look at what happened
bowtie2-build reference.fasta reference
ls

# Next, we align our reads to our reference and look at the resulting file
bowtie2 -x reference -U short_reads.fastq -S assembly.sam
nano assembly.sam

# Then we convert the assembly to a sorted binary file (BAM) using samtools
samtools view -bS assembly.sam | samtools sort -o assembly.bam

# Finally we index our BAM file
# Don't forget to look at the file sizes
samtools index assembly.bam
ls -l

# Now we can look at the final result using tablet
# "Open Assembly" -> Select the BAM file as the assembly and the fasta file as the reference
tablet

```

|||

### [SAM/BAM format](https://samtools.github.io/hts-specs/SAMv1.pdf)

* &shy;<!-- .element: class="fragment" -->The "standard" way to represent assembled data
    * &shy;<!-- .element: class="fragment" -->Contain the reads and their coordinates relative to a reference / each other
    * &shy;<!-- .element: class="fragment" -->A BAM file is a binary version of a SAM file
    * &shy;<!-- .element: class="fragment" -->BAM files can be indexed

---

### Fun with assemblies!

* &shy;<!-- .element: class="fragment" -->Once we have a SAM/BAM file there is a lot we can do
    * &shy;<!-- .element: class="fragment" -->Get coverage data (AKA depth)
    * &shy;<!-- .element: class="fragment" -->Call variants
  
&shy;<!-- .element: class="fragment" -->![The execution of Lord Eddard Stark, from GoT](assets/fun.jpg)

|||

### Fun with assemblies!

```bash
conda activate hts
conda install bcftools

bcftools mpileup --fasta-ref reference.fasta assembly.bam | bcftools call -m -v - > Variants.vcf
# This command will output a new file, where all variant positions are logged

```

|||

### More on VCF?


* &shy;<!-- .element: class="fragment" -->[**V**ariant **C**alling **F**ormat](https://samtools.github.io/hts-specs/VCFv4.2.pdf)
 * &shy;<!-- .element: class="fragment" -->Contains data on variants
 * &shy;<!-- .element: class="fragment" -->Very compact format

---

### What if there is no reference?

* &shy;<!-- .element: class="fragment" -->The problem is now **a lot** harder
    * &shy;<!-- .element: class="fragment" -->We need to align our reads to each other
    * &shy;<!-- .element: class="fragment" -->There are also plenty of algorithms for this
        * &shy;<!-- .element: class="fragment" -->Spades
        * &shy;<!-- .element: class="fragment" -->Trinity
        * &shy;<!-- .element: class="fragment" -->...many more!

&shy;<!-- .element: class="fragment" -->![Arya Stark and Syrio Forell from GoT sparing](assets/Not_today.png)

---

### Obtaining HTS data

* &shy;<!-- .element: class="fragment" -->Meet the [SRA database](https://www.ncbi.nlm.nih.gov/sra)
    * &shy;<!-- .element: class="fragment" -->**S**equence **R**ead **A**rchive
    * &shy;<!-- .element: class="fragment" -->Contains *raw* sequence data
    * &shy;<!-- .element: class="fragment" -->Search for "phage"
    * &shy;<!-- .element: class="fragment" -->There is something missing though...

&shy;<!-- .element: class="fragment" -->![Picture of a Data Center](assets/data_center.jpg)

|||

### SRA tools

* &shy;<!-- .element: class="fragment" -->In order to get data from SRA you need a [toolkit](https://github.com/ncbi/sra-tools)
    * &shy;<!-- .element: class="fragment" -->`sra-tools`
    * &shy;<!-- .element: class="fragment" -->Contains a lot of tools to handle SRA data
    * &shy;<!-- .element: class="fragment" -->For now we will focus on `fasterq-dump`

&shy;<!-- .element: class="fragment" -->![Picture of weapons and armor used in GoT](assets/tools.jpg)

|||

### Getting data

```bash
conda create -n sra  # SRA-tools conflicts with other packages
conda activate sra
conda install sra-tools

mkdir sra_seqs
cd sra_seqs
fasterq-dump "RUN_ACCESSION"
# Note that RUN_ACCESSION is different from ACCESSION
```

---

### Practical work

* Obtain HTS data from SRA for a phage of your choice
* Obtain the genome of this phage from NCBI genomes database
* Map your HTS data to the reference genome
* Obtain the average coverage of your genome
* Find which position has the highest coverage
* Obtain the average coverage of the 200 base pairs around the highest coverage position (100 before + 100 after, a total of 201 positions)
* Perform a basic SNP calling on your assembled genome (between your HTS data and the reference sequence)

---

### References

* [High Throughput Sequencing techs](https://dx.doi.org/10.1016%2Fj.molcel.2015.05.004)
* [Bowtie2 Tutorial](https://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#getting-started-with-bowtie-2-lambda-phage-example)
* [BWA](https://bio-bwa.sourceforge.net/)
* [SamTools Documentation](https://www.htslib.org/doc/)
* [**V**ariant **C**alling **F**ormat](https://samtools.github.io/hts-specs/VCFv4.2.pdf)
* [Variant calling](https://wikis.utexas.edu/display/bioiteam/Variant+calling+using+SAMtools)
* [Spades](https://cab.spbu.ru/software/spades/)
* [SRA Documentation](https://www.ncbi.nlm.nih.gov/books/NBK56551/)
